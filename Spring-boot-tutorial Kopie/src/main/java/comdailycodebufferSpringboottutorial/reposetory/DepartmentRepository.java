package comdailycodebufferSpringboottutorial.reposetory;

import comdailycodebufferSpringboottutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Long> {

    // Sucht den Passenden Namen die Daten die zu einem bestimmten Namen gehören und geben diesen Zurück.
    public Department findByDepartmentName(String departmentName);
    // Sucht gleich wie oben nach den Entsprechenden Daten ignoriert jedoch groß und Keleinschreibung.
    @Query(value = "",nativeQuery = true)
    public Department findByDepartmentNameIgnoreCase(String departmentName);
}
