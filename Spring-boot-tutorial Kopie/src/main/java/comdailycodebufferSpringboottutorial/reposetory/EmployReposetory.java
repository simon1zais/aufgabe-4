package comdailycodebufferSpringboottutorial.reposetory;

import comdailycodebufferSpringboottutorial.entity.Employ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployReposetory extends JpaRepository<Employ,Long> {

    public Employ findByNameIgnoreCase(String employName);
}
