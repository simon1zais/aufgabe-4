package comdailycodebufferSpringboottutorial.controller;
import comdailycodebufferSpringboottutorial.entity.Employ;
import comdailycodebufferSpringboottutorial.service.EmployService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EmployController {

    @Autowired
    private EmployService employService;

    @PostMapping("/employ")
    public Employ saveEmploy(@Valid @RequestBody Employ employ){
        return employService.saveEmploy(employ);

    }

    @GetMapping("/employ")
    public List<Employ> fetchemployList(){
        return employService.fetchEmployList();
    }

    @DeleteMapping("/employ/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long employId){

        employService.deleteEmployById(employId);

        return "Der Employ mit der Id: " + employId + " wurde erfolgreich Gelöscht!!";

    }

    @GetMapping("/departments/name{name}")
    public Employ fetchEmployByName(@PathVariable("name") String employName){

        return employService.fetchEmployByName(employName);

    }
}









