package comdailycodebufferSpringboottutorial.controller;

import comdailycodebufferSpringboottutorial.Error.DepartmantNotFountExeption;
import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.service.DepartmentService;
import comdailycodebufferSpringboottutorial.service.DepartmentServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    //Mit dieser Variable können logs erstellt werden die mit bestimmten einstellungen in der Console oder in einer Datei ausgegeben werden können.
    private final Logger log= LoggerFactory.getLogger(DepartmentController.class);

    @PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department){
        log.info("In saveDepartment von DepartmentController");
        return departmentService.saveDepartment(department);

    }

    //beim Aufrufen dieser Methode werden alle eingetargenen departments zurückgegeben.

    @GetMapping("/departments")
    public List<Department> fetchDepartmentList(){
        log.info("In fetchDepartmentList von DepartmentController");
        return departmentService.fetchDepartmentsList();
    }

    // gibt das Department mit der Entsprechende Id zurück
    @GetMapping("/departments/{id}")
    public Department fetchDepartmentById(@PathVariable("id") Long departmentId) throws DepartmantNotFountExeption {

        return departmentService.fetchDepartmentById(departmentId);
    }

    // Löscht den entsprechenden Eintrag mit der entsprechenden ID.
    @DeleteMapping("/departments/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long departmentId){

         departmentService.deleteDepartmentById(departmentId);

         return "Das Department mit der Id: " + departmentId + " wurde erfolgreich Gelöscht!!";

    }

    // ändert die Daten in einen Bestehenden Eintrag ab
   @PutMapping("/departments/{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId, @RequestBody Department department){

        return departmentService.updateDepartment(departmentId,department);

    }

    //Sucht Department anhand des Namens

    @GetMapping("/departments/name{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName){

        return departmentService.fetchDepartmentByName(departmentName);

    }
}
