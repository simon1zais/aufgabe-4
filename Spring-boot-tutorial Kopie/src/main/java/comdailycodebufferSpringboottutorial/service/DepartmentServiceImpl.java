package comdailycodebufferSpringboottutorial.service;

import comdailycodebufferSpringboottutorial.Error.DepartmantNotFountExeption;
import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.reposetory.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService{

    // Stellt eine vrebindung zu der Klasse Department Reposetory her um seine specialen methoden nutzen zu können
    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public List<Department> fetchDepartmentsList() {
        return departmentRepository.findAll();
    }

    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmantNotFountExeption {
        Optional<Department> department = departmentRepository.findById(departmentId);

        if(!department.isPresent()){

            throw new DepartmantNotFountExeption("Department nicht vorhanden");
        }

        return department.get();
    }

    @Override
    public void deleteDepartmentById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }

    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department deDB = departmentRepository.findById(departmentId).get();

        if(Objects.nonNull(department.getDepartmentName()) && !"".equalsIgnoreCase(department.getDepartmentName())){

            deDB.setDepartmentName(department.getDepartmentName());


        }
        if(Objects.nonNull(department.getDepartmentCode()) && !"".equalsIgnoreCase(department.getDepartmentCode())){

            deDB.setDepartmentCode(department.getDepartmentCode());


        }
        if(Objects.nonNull(department.getDepartmentAddress()) && !"".equalsIgnoreCase(department.getDepartmentAddress())){

            deDB.setDepartmentAddress(department.getDepartmentAddress());

        }

        return departmentRepository.save(deDB);

    }

    @Override
    public Department fetchDepartmentByName(String departmentName) {
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }

}
