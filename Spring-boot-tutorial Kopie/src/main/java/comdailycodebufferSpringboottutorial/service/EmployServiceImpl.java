package comdailycodebufferSpringboottutorial.service;

import comdailycodebufferSpringboottutorial.Error.DepartmantNotFountExeption;
import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.entity.Employ;
import comdailycodebufferSpringboottutorial.reposetory.EmployReposetory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployServiceImpl implements EmployService {

    @Autowired
    private EmployReposetory employReposetory;

    @Override
    public Employ saveEmploy(Employ employ) {
        return employReposetory.save(employ);
    }

    @Override
    public List<Employ> fetchEmployList() {
        return employReposetory.findAll();
    }

    @Override
    public void deleteEmployById(Long employId) {
        employReposetory.deleteById(employId);
    }

    @Override
    public Employ fetchEmployByName(String employName) {
        return employReposetory.findByNameIgnoreCase(employName);
    }

}
