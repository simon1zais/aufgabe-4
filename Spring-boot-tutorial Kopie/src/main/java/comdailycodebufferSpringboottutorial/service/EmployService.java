package comdailycodebufferSpringboottutorial.service;

import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.entity.Employ;

import java.util.List;

public interface EmployService {

    public Employ saveEmploy(Employ employ);
    public List<Employ> fetchEmployList();
    public void deleteEmployById(Long employId);
    public Employ fetchEmployByName(String employName);
}
