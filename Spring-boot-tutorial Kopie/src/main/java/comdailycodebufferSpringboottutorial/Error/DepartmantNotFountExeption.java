package comdailycodebufferSpringboottutorial.Error;

public class DepartmantNotFountExeption extends Exception {

    public DepartmantNotFountExeption() {
    }

    // Wenn ein Department nicht gefunden wurde so gibt es die jeweilige Nachricht aus.

    public DepartmantNotFountExeption(String message) {
        super(message);
    }

    public DepartmantNotFountExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public DepartmantNotFountExeption(Throwable cause) {
        super(cause);
    }

    public DepartmantNotFountExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
