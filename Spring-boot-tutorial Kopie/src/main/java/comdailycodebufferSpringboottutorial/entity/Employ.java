package comdailycodebufferSpringboottutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Employ {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long Id;

    @NotBlank(message = "Bitte den Vornamen eintragen!!")
    private String name;
    @NotBlank(message = "Bitte den Nachnamen eintragen!!")
    private String lastName;
    @NotBlank(message = "Bitte E-Mail eintragen!!")
    private String email;

}
