package comdailycodebufferSpringboottutorial.controller;

import comdailycodebufferSpringboottutorial.Error.DepartmantNotFountExeption;
import comdailycodebufferSpringboottutorial.entity.Department;
import comdailycodebufferSpringboottutorial.service.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DepartmentService departmentService;

    private Department department;

    @BeforeEach
    void setUp() {

        Department department =
                Department.builder().departmentName("Plumber").departmentAddress("China").departmentCode("Test-3").departmentId(1L).build();

    }

    @Test
    void saveDepartment() throws Exception {
        Department inputDepartment =
                Department.builder().departmentName("Plumber").departmentAddress("China").departmentCode("Test-3").build();
        Mockito.when(departmentService.saveDepartment(inputDepartment));

        mockMvc.perform(post("/departments")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                "\t\"departmentName\":\"Plumber\",\n" +
                "\t\"departmentAddress\":\"China\",\n" +
                "\t\"departmentCode\":\"Test-3\"\n" +
                "\t\n" +
                "}\n")).andExpect(status().isOk());

    }

    @Test
    void fetchDepartmentById() throws Exception {
        Mockito.when(departmentService.fetchDepartmentById(1L)).thenReturn(department);

        mockMvc.perform(get("/departments/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$.departmentName").value(department.getDepartmentName()));
    }
}